import React, { useEffect } from 'react';
import View from './view';
import { setUserData } from '../../store/reducers/user-reducer';
import { connect } from 'react-redux';
import { WeatherService } from '../../services';


const Container = (props) => {
  useEffect(() => {
    props.setUserData({name: 'Ilya'})
    WeatherService.getWeather('Minsk')
  }, [ ])

    return (
        <View />
    );
}


const mapStateToProps = state => ({
  userData: state.user.userData,
});
const mapDispatchToProps = dispatch => ({
  setUserData: userData => dispatch(setUserData(userData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Container);
