import { Switch, Route, Redirect } from 'react-router-dom';
import { Home } from './pages'
import './App.css';

const App = () => {
  return (
      <Switch>
        <Route exact path="/home">
          <Home />
        </Route>
        <Redirect from="*" to="/home" />
      </Switch>
  );
}

export default App;
