import { apiGet } from '../api';

export default {
    async getWeather(q) {
        try {
            const result = await apiGet('/weather', {params: {q}});
            return ({ data: result.data, status: result.status });
        } catch (error) {
            throw new Error(error.response.data.message);
        }
    },
}